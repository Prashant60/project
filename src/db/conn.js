const mongoose = require("mongoose");
mongoose
  .connect("mongodb://localhost:27017/dbRegister", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => console.log("DB Connected"))
  .catch((e) => console.log("No Connection"));
