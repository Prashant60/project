require("dotenv").config();
const express = require("express");
const app = express();
require("./db/conn");
const Register = require("./models/registers");
const upload = require("./db/upload");
const path = require("path");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const cookieParser = require("cookie-parser");
const auth = require("./middleware/auth");
const fs = require("fs");

const port = process.env.PORT || 8000;
app.use(
  session({ secret: "bc7t84m8349884m", resave: false, saveUninitialized: true })
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/uploads", express.static("uploads"));

app.set("view engine", "hbs");

// const createToken = async() => {
//   jwt.sign({}, )
// }

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/home", auth, (req, res) => {
  const fpath = path.join("../uploads/", path.basename(req.user.avatar));

  res.render("home", {
    name: req.user.name,
    file: fpath,
  });

  // app.get("/download", auth, (req, res) => {
  //   const fpath2 = path.join("./uploads/", path.basename(req.user.avatar));
  //   res.download(fpath2);
  // });
});
app.get("/download", auth, (req, res) => {
  const fpath2 = path.join("./uploads/", path.basename(req.user.avatar));
  res.download(fpath2);
});

app.get("/logout", auth, async (req, res) => {
  try {
    // single device logout
    // req.user.tokens = req.user.tokens.filter((currentElement) => {
    //   return currentElement.token !== req.token;
    // });

    //Logout of all devices
    req.user.tokens = [];

    res.clearCookie("jwt");
    console.log("logged out");
    await req.user.save();
    res.redirect("login");
  } catch (error) {
    res.status(500).send(error);
  }
});

app.get("/login", async (req, res) => {
  res.render("login");
});

app.post("/", upload.upload.single("imageFile"), async (req, res) => {
  try {
    const registerData = new Register({
      name: req.body.Name,
      email: req.body.Email,
      password: req.body.Pass,
      contact: req.body.Contact,
    });
    if (req.file) {
      registerData.avatar = req.file.path;
    }

    const token = await registerData.generateAuthToken();

    res.cookie("jwt", token, {
      // expires: new Date(Date.now() + 600000),
      httpOnly: true,
    });

    const registered = await registerData.save();

    res.status(201).render("index");
  } catch (error) {
    res.status(400).send(error);
  }
});

app.post("/login", async (req, res) => {
  try {
    const emailId = req.body.Email;
    const pwd = req.body.Pass;

    const validUser = await Register.findOne({ email: emailId });

    const token = await validUser.generateAuthToken();
    console.log("The token part: " + token);

    res.cookie("jwt", token, {
      expires: new Date(Date.now() + 600000),
      httpOnly: true,
      //secure:true
    });

    if (validUser.password === pwd) {
      req.session.validUser = validUser;
      res.status(201).redirect("home");
    } else {
      res.send("Incorrect Password");
    }
  } catch (error) {
    res.status(400).send("Invalid Email");
  }
});

app.listen(port, () => {
  console.log(`Server is connected at port no ${port}`);
});
